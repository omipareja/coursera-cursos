'use strict';

module.exports = function (grunt){
      require('time-grunt')(grunt);
      require('jit-grunt')(grunt,{
          useminPrepare:'grunt-usemin'
      });
// aca vinen todas las tareas
      grunt.initConfig({
          sass:{
            dist:{
              files:[{
                  expand:true,
                  cwd: 'css',// de la carpeta css
                  src: ['*.scss'], // los archivos con esta extencion
                  dest: 'css', // carpeta destino
                  ext: '.css' // a esta extencion
              }]
            }
          },

          watch:{
            files:['css/*.scss'], //cuando un archivo con esta extencion cambia
            tasks: ['css'] // se ejecuta esta instruccion que es el nombre de la tarea sass
          },
            browserSync:{
              dev: {
                bsFiles:{//browser files
                  src:[
                    'css/*.css', // archivos que monitorea para cambios
                    '*.html',
                    'js/*.js'
                  ]
                },
                options: {
                  watchTask:true,
                  server:{
                    baseDir: './' //Directorio base para nuestro servidor
                  }
                }
              }
            },

            imagemin: {
              dynamic: {
                files: [{
                    expand: true,
                    cwd:'./', // revisar dentro del proyecto
                    src: 'img/*.{png,gif,jpg,jpeg}', // revisar todos los archivos de la carpeta
                    dest: 'dist' //mandarla a dist
                }]
              }
            },

          copy: {
            html: {
              files: [{
                expand: true ,
                dot:  true,
                cwd: './', // que estan en la raiz
                src: ['*html'], // pasa los archivos html que indicamos
                dest: 'dist'
              }]
            },
            fonts:{
              files: [{
                //font-awesome
                expand:true,
                dot:true,
                cwd: 'node_modules/open-iconic/font',
                src:['fonts/*.*'], //busque todo su contenido
                dest: 'dist'
              }]
            }
          },

          clean: {
            build:{
              src:['dist/'] //eliminar carpeta dist
            }
          },

          cssmin:{
            dist: {}
          },

          uglify:{
            dist:{}
          },

          filerev:{ //genera codigo para que no sea cachable
            options:{
              encording: 'utf8',
              algorithm: 'md5',
              length: 20
            },

            release:{

              files:[{
                src: [
                  'dist/js/*.js',
                  'dist/css/*.css'
                ]
              }]
            }
          },

          concat:{
            options:{
              separator: ';'
            },
            dist:{}
          },

          useminPrepare: { // les aplica el proceso de min para guardarlos en dist
            foo:{
              dest:'dist',
              src: ['index.html','about.html','precios.html','contacto.html']
            },
            options:{
              flow:{
                steps: {
                  css: ['cssmin'],
                  js: ['uglify']
                },
                post:{
                  css:[{
                    name:'cssmin',
                    createConfig: function(context,block){
                      var generated = context.options.generated;
                      generated.options = {
                        keepSpecialComments:0,
                        rebase: false
                      }
                    }
                  }]
                }
              }
            }
          },

          usemin: { // busca en estos archivos y toma los acces ahi de
            html:['dist/index.html','dist/about.html','dist/precios.html','dist/contacto.html'],
            options:{
              assetsDir: ['dist','dist/css','dist/js']
            }
          }
      });
      //  grunt.loadNpmTasks('grunt-contrib-watch');
      //  grunt.loadNpmTasks('grunt-contrib-sass');
      //  grunt.loadNpmTasks('grunt-browser-sync');
      //  grunt.loadNpmTasks('grunt-contrib-imagemin')
        grunt.registerTask('css',['sass']);
        grunt.registerTask('default',['browserSync','watch']);
        grunt.registerTask('img:compress',['imagemin']);
        grunt.registerTask('build',[
              'clean',
              'copy',
              'imagemin',
              'useminPrepare',
              'concat',
              'cssmin',
              'uglify',
              'filerev',
              'usemin'

        ])
};
